/*

This is the script that fetches the emoji when the user submits the shortcode, and then displays the emoji in the emoji box. It also handles the error when the response from the server is not okay, for instance, when there is a network issue.

*/

function getEmoji() {
    let shortcode = document.getElementById('shortcode').value;
    fetch(`http://127.0.0.1:8080/emoji/${shortcode}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.text();
        })
        .then(emoji => {
            // Only display the emoji if it's not "null"
            if (emoji !== 'null') {
                document.getElementById('emoji').textContent = emoji;
            } else {
                document.getElementById('emoji').textContent = `No emoji found for ${shortcode}`;
            }
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}


