- Project Name: Emojinator

- Authors: Gurleen Dhaliwal & Niraja Pullagoru

- Libraries: Actix Web Server and Emojis


- Description of the project:

Emojinator generates corresponding emojis based on an emotion or occasion that is entered in the input text box. 
This project is created in Rust using Actix-web as the web framework. 

The input tag will be processed using the Rust library emojis and the corresponding emoji will be displayed.

It allows users to input an emoji keyword and returns the corresponding emoji character. If an input does not have a corresponding emoji, it informs the user that no emoji was found for their keyword.

Also, has an added functionality of saving the emojis as as a PNG.



- Building and Running the Project:

First, you will need to have Rust installed on your machine. If you don't have Rust installed, you can download it from the official website: https://www.rust-lang.org/tools/install.

- To build and run the project, navigate to the project root directory in your terminal and use the following commands:

- To compile: cargo build

- To run: cargo run

- Once you do Cargo run - Visit Localhost to see website: http://127.0.0.1:8080/

- Enter any emoji keyword like smile, balloon, rocket.

Visit this to see webpage after running program in terminal: http://127.0.0.1:8080/



- Testing

Testing was done using the inbuilt Rust testing framework. We have unit tests to check if the function handling the emoji keyword returns the right response for both existing and non-existing emoji keywords.

To run tests, use the following command in your terminal:

- cargo test

- Libraries Used:

actix-web = "3.3.2"
emojis = "0.5.2"
actix-files = "0.5.0" 

Note: Actix-files uses the library nom as a depnedancy which may be not acceptable in the upcoming versions of rust.

- Conclusion

The implementation of the project was successful, and the server operates as expected. The main challenge was handling and returning the right HTTP responses. I have also added the functionality to save any emojia s a PNG in own device through the website.

For future improvements, I would like to implement a more intuitive user interface and add more emojis.



