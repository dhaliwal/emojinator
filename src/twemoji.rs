use twemoji_rs::get_twemoji;

// Our function to get the icon corresponding to a certain emoji
fn get_icon() -> Result<(), Box<dyn std::error::Error>> {
    // If the emoji exists, we open and decode its corresponding image file
    if let Some(path_to_icon) = twemoji_rs::get_twemoji("🚀") {
        let img = image::io::Reader::open(path_to_icon)?.decode()?;
        // We can do whatever we want with img here...
        Ok(())
    } else {
        // If the emoji doesn't exist, we print an error message
        println!("Couldn't find an icon file :(");
        Ok(())
    }
}

// We define some tests for our get_icon function.
#[cfg(test)]
mod tests {
    use super::*;

    // A test to check if our get_icon function returns an Ok result.
    #[test]
    fn test_get_icon() {
        assert!(get_icon().is_ok());
    }
}
