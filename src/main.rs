use actix_files::Files;
use actix_web::{web, App, HttpServer, Responder};
use emojis::get_by_shortcode;

// Our async function to get the emoji when provided with a shortcode.
async fn get_emoji(info: web::Path<(String,)>) -> impl Responder {
    // We get the shortcode provided in the URL
    let (keyword,) = info.into_inner();
    // We convert it to lowercase to ensure it matches the shortcodes we have.
    let _keyword_lower = keyword.to_ascii_lowercase();
    // We match on the result of get_by_shortcode function.
    // If the shortcode exists, we return the corresponding emoji.
    // If not, we return a not found message.
    match get_by_shortcode(&_keyword_lower) {
        Some(e) => format!("{}", e),
        None => format!("No emoji found for {}", keyword),
    }
}

// The main function where we initialize our server and define our routes
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            // The /emoji/{keyword} route, which uses our get_emoji function to serve responses
            .service(web::resource("/emoji/{keyword}").to(get_emoji))
            // A route to serve our static files from the / directory.
            .service(Files::new("/", "./static/").index_file("index.html"))
    })
    // We bind our server to localhost at port 8080.
    .bind("127.0.0.1:8080")?
    // We run our server asynchronously.
    .run()
    .await
}

// We define some tests for our get_emoji function.
#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::http::StatusCode;
    use actix_web::test;

    // A test to check if our get_emoji function returns a successful response.
    #[actix_rt::test]
    async fn test_get_emoji() {
        let mut app =
            test::init_service(App::new().route("/emoji/{keyword}", web::get().to(get_emoji)))
                .await;
        let req = test::TestRequest::get().uri("/emoji/smile").to_request();
        let resp = test::call_service(&mut app, req).await;
        assert!(resp.status().is_success());
    }

    // A test to check if our get_emoji function returns a successful response when the emoji exists.
    #[actix_rt::test]
    async fn test_get_emoji_exists() {
        let mut app =
            test::init_service(App::new().service(web::resource("/emoji/{keyword}").to(get_emoji)))
                .await;

        let req = test::TestRequest::with_uri("/emoji/rocket").to_request();
        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::OK);
    }

    // A test to check if our get_emoji function returns a successful response even when the emoji does not exist.
    #[actix_rt::test]
    async fn test_get_emoji_not_exists() {
        let mut app =
            test::init_service(App::new().service(web::resource("/emoji/{keyword}").to(get_emoji)))
                .await;

        let req = test::TestRequest::with_uri("/emoji/nonexistentemoji").to_request();
        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::OK);
    }
}
